var juego = new Phaser.Game(960, 600, Phaser.AUTO, '', {
  preload: preload,
  create: create,
  update: update,
});

let jugadorStats = {
  velocidad: 200,
  puntaje: 0,
  salto: 0,
  jugadorTieneLlave: false,
};

let cursores;

function preload() {
  juego.load.image('plataforma_8x1', '/imagenes/Norte/plataforma_8x1.png');
  juego.load.image('piso', '/imagenes/Norte/piso.png');
  juego.load.image('fondo', '/imagenes/Norte/fondo.png');
  juego.load.image('personajePrincipal', '/imagenes/Norte/icono_jugador_h.png');
  juego.load.spritesheet('pickup', 'imagenes/Pampa/pickup.png', 33, 38);
  juego.load.spritesheet('jugador', 'imagenes/Pampa/jugador_h.png', 33, 49);
}

function create() {
  juego.physics.startSystem(Phaser.Physics.ARCADE);
  juego.physics.arcade.gravity.y = 1200;
  cursores = juego.input.keyboard.createCursorKeys();

  juego.add.image(0, 0, 'fondo');

  plataformas = juego.add.group();
  plataformas.enableBody = true;

  var plata8x1Left = plataformas.create(0, 300, 'plataforma_8x1');
  var plata8x1Right = plataformas.create(600, 300, 'plataforma_8x1');
  var piso = plataformas.create(0, 550, 'piso');

  for (let i = 0; i < plataformas.children.length; i++) {
    FijarEnLugar(plataformas.children[i]);
  };

  pickup = juego.add.sprite(200, 263, 'pickup');
  pickup.animations.add('rotar', [0, 1, 2, 3], 6, true);
  pickup.animations.play('rotar');

  //Dibuja al jugador y le activa la fisica y animacion
  jugador = juego.add.sprite(21, 485, 'jugador');
  juego.physics.arcade.enable(jugador);
  jugador.anchor.set(0.5, 0.5);
  jugador.animations.add('ocioso', [0]);
  jugador.animations.add('correr', [1, 2], 8, true);
  jugador.animations.add('saltar', [3]);
  jugador.animations.add('caer', [4]);

  jugador.body.collideWorldBounds = true;

}

function update() {
  juego.physics.arcade.collide(jugador, plataformas);

  if (cursores.left.isDown) {
    mover(-1);
  } else if (cursores.right.isDown) {
    mover(1);
  } else {
    jugador.animations.play('ocioso');
    jugador.body.velocity.x = 0;
  }

  if (jugador.body.velocity.x != 0) {
    jugador.animations.play('correr');
  }
}

function FijarEnLugar(objeto) {
  objeto.body.moves = false;
  objeto.body.immovable = true;
};

function mover(direction) {
  jugador.body.velocity.x = jugadorStats.velocidad * direction;
  jugador.scale.x = direction;
};
